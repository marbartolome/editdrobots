# https://code.google.com/codejam/contest/975485/dashboard
import pprint

class Action(object):
  def __init__(self, robId, task, pos):
    self.robId = robId
    self.task = task
    self.pos = pos

  def __str__(self):
    return "Action({},{},{})".format(self.robId, self.task, self.pos)

  def __repr__(self):
    return self.__str__()

  def __eq__(self, other):
    return (self.robId==other.robId and self.task==other.task and self.pos==other.pos)

class Goal(Action):
  def __init__(self, robotId, task, pos):
    super(Goal,self).__init__(robotId, task, pos)
    self.done = False

class History(list):

  def add(self, actions):
    self.append(actions)

  def get_iter(self, i):
    return [action for action in self[i].values()]

  def meets_preconditions(self, preconds):
    return self.goals_to_date()[0:len(preconds)] == preconds
  
  def goals_to_date(self):
    return [ action for step in self for action in step if action.task=='push']


class Overmind(object):
  def __init__(self, robots, goals):
    self.robots = robots
    self.goals = goals
    self.history = History()

  def next_goal_for_robot(self,robId):
    for i, goal in enumerate(self.goals):
      if (not goal.done) and goal.robId==robId:
        return (goal, self.goals[0:i])
    return (None, self.goals)

  def finished(self):
    return False not in [goal.done for goal in self.goals]

  def step(self):
    step_actions = []
    for robot in self.robots:
      next_goal, preconds = self.next_goal_for_robot(robot.name)
      if next_goal:
        # update
        pos_diff = next_goal.pos - robot.pos
        if pos_diff > 0:
          step_actions.append(robot.move_forward())
        elif pos_diff < 0:
          step_actions.append(robot.move_backwards())
        else:
          if self.history.meets_preconditions(preconds):
            step_actions.append(robot.push())
            next_goal.done = True
          else:
            step_actions.append(robot.wait())
      
      else:
        step_actions.append(robot.wait())
        
    self.history.add(step_actions)


class Robot(object):

    def __init__(self,name):
        self.name = name
        self.pos = 1

    def move_forward(self):
      self.pos += 1
      return Action(self.name, 'move', self.pos)

    def move_backwards(self):
      self.pos -= 1
      return Action(self.name, 'move', self.pos)

    def push(self):
      return Action(self.name, 'push', self.pos)

    def wait(self):
      return Action(self.name, 'wait', self.pos)


if __name__ == "__main__":
    steps = [ ('O',2), ('B', 1), ('B',2), ('O', 4)]

    goals = [Goal(step[0], 'push', step[1]) for step in steps]
    
    overmind = Overmind([Robot('O'), Robot('B')], goals)

    while not overmind.finished():
      overmind.step()

    pprint.pprint(overmind.history)
